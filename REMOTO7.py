#Questão1
def filtraMultiplos(listaBelha,n):
    '''função que filtra os numeros multiplos de um numero N
    list, int -> list'''
    listaNueba = list()
    ind = 0
    while ind<len(listaBelha):
        if listaBelha[ind]%n == 0:
            
            listaNueba.append(listaBelha[ind])
        ind = ind + 1  
        
    return listaNueba

#Questão2
def uppCons(frase):
    '''Função que transforma todas as consoantes da frase em maiúsculas
    string -> string'''
    i = 0
    fraseNoba = ''
    
    while i<len(frase):
        
        if frase[i] in 'bcdfghjklmnpqrstvwxyzç': 
            fraseNoba = fraseNoba + frase[i].upper()
        else:
            fraseNoba = fraseNoba + frase[i]

        
        i = i + 1    
        
    return fraseNoba

#Questão3
def posLetra(string,letra,num):
    '''Função que retorna a posição da ocorrencia "num" de uma letra na frase
    string, string, int -> int'''
    i = 0
    lista = []
    while i<len(string):
        if string[i] == letra:
            lista.append(i)
            #Adiciona na lista um apêndice quando i = posiçãio da ocorrência da letra na string
        i = i + 1
        
    
    if len(lista) >= num:
        return lista[num-1]
    if len(lista)<num:
        return -1

#Questão4
def repetidos(lista):
    '''Função que retorna o numero de vezes que um elemento da lista é igual ao elemento anterior
    list -> int'''
    i = 1
    score = 0
    while i<len(lista):
        if lista[i-1] == lista[i]:
            score = score + 1 
        i = i + 1
    return score

#Questão5
def fatorial(n):
    '''Função que retorna o fatorial de um número
    int -> int'''
    i = 1
    fatorial = 1
    auxiliar = n
    while i<n:
       auxiliar = auxiliar*i 
       i = i + 1
    return auxiliar

#Questão6
def faltante(L):
    '''Função que retorna o número pertencente ao intervalo [1,N] que já está na lista 
    list -> int'''
    list.sort(L)
    i = len(L)
    listaCerta = list(range(1,i+2))
    listaFaltantes = []
    h = 0
    while h+1<len(listaCerta):
        if L[h] != listaCerta[h]:
            return listaCerta[h]
        h = h + 1
    return listaCerta[-1]


